This App transmits no data over the internet. All data is stored on the device.

Only data important for the theatre performance is stored, including: Current Points, current Game-Room and Group-Number

The Camera-Permission is required for scanning the QR codes important for the functioning of the game.
The QR-Code library used is the following: https://pub.dartlang.org/packages/barcode_scan